import React, { Component } from 'react';

class App extends Component {
  state = {
    todos: []
  }

  componentDidMount() {
    fetch('http://localhost:8080/employees/consultAll')
    .then(res => res.json())
    .then((data) => {
      this.setState({ todos: data })
      console.log(this.state.todos)
    })
    .catch(console.log)
  }
  render() {
    
    return (
      <div className="container">
      <div className="col-xs-12">
      <h1>Empresa Ejemplo</h1>
      <button>Consultar </button>
          <button>Agregar </button>
          <button>Actualizar </button>
          <button>Eliminar </button>
          {this.state.todos.map((todo) => (
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">{todo.name}</h5>
            </div>
          </div>
        ))}
      </div>
     </div>
    );
  }
}


export default App;